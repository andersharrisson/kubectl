FROM centos:7

MAINTAINER Anders Harrisson <anders.harrisson@esss.se>

RUN rpm --import https://packages.cloud.google.com/yum/doc/yum-key.gpg \
 && rpm --import https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg \
 && yum-config-manager --add-repo https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64 \
 && yum install -y kubectl
